<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;

class TaskController extends Controller
{

    protected function getUserList($users, $key = 'name') {
        $input = [];

        foreach ($users as $user) {
            $input[$user->id] = $user->{$key};
        }

        return $input;
    }
    public function getList() {

        // orderBy('id', 'asc')
        // orderBy('id', 'desc')
        $tasks = \App\Task::orderBy('id', 'desc');

        $users = User::get();

        $input = $this->getUserList($users);

        $params = (object) request()->all();

        if (request()->filled('name')) {
            $tasks = $tasks->where('name', 'like', "%" . request()->name . "%");
        }

        if (request()->filled('user_id')) {
            $tasks = $tasks->where('user_id', request()->user_id);
        }

        if (request()->filled('email')) {
            $tasks = $tasks->where('user_id', request()->email);
        }



        $tasks = $tasks->paginate(10);

        $inputEmail = $this->getUserList($users, 'email');


        return view('tasks.index', compact('tasks', 'users', 'params', 'input', 'inputEmail'));

    }

    public function getCreateTask() {

        $users = User::get();

        $input = $this->getUserList($users);

        return view('tasks.create', compact('users', 'input'));
    }

    public function getViewTask($id) {

        $task = Task::find($id);

        $users = User::get();

        $input = $this->getUserList($users);


        return view('tasks.view', compact('users', 'task', 'input'));
    }

    public function postCreateTask() {

        $task = new Task();
        $task->name = request()->name;
        $task->content = request()->get('content');
        $task->active = request()->has('active');
        $task->category = request()->category;
        $task->user_id = request()->user_id;
        $task->save();

        return redirect()->to("/tasks/$task->id")->with('status', 'Success');
    }

    public function postDelete($id) {

        Task::where('id', $id)->delete();

        return redirect()->back()->with('status', 'Success');

    }
}
