<form method="POST" action="/tasks/create">

    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{isset($task)? $task->name : ''}}">
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Content</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="content">{{isset($task)? $task->content : ''}}</textarea>
    </div>

    <div class="form-group">
        <label for="exampleFormControlSelect1">Category</label>
        {{Form::select('category', ['main' => 'Main', 'secondary' => 'Secondary'],isset($task)? $task->category: '', ['class' => 'form-control'])}}

    </div>

    <div class="form-group">
        <label for="exampleFormControlSelect1">Assigned to:</label>

        {{Form::select('user_id', $input,isset($task)? $task->user_id: '', ['class' => 'form-control'])}}

    </div>


    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1" name="active" {{isset($task) && $task->active == 1? "checked" : ''}}>
        <label class="form-check-label" for="exampleCheck1">Active</label>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
