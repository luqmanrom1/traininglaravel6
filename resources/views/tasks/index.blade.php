@extends('layouts.app')

@section('content')
    <div class="container">


        <div class="row justify-content-center mb-4">

            @if(session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success</strong> {{session('status')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="col-md-10">
                <button class="btn btn-secondary" data-toggle="modal" data-target="#exampleModalCenter">Filter</button>
                <a class="btn btn-primary" href="/tasks/create">Create</a>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">List of tasks</div>

                    <div class="card-body">

                        @if($tasks->isEmpty())
                            <p> There is no tasks</p>
                        @else

                            <table class="table">
                                <thead>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Category</th>
                                    <th>Owner</th>
                                    <th>Action</th>
                                </thead>

                                <tbody>
                                    @foreach($tasks as $task)
                                        <tr>
                                            <td>{{($tasks->currentPage() - 1) * $tasks->perPage() + $loop->index + 1}}</td>
                                            <td>{{$task->id}}</td>
                                            <td>{{$task->name}}</td>
                                            <td>{{$task->content}}</td>
                                            <td>{{$task->category}}</td>
                                            <td>{{$task->user->name}}</td>
                                            <td style="display: flex;flex-direction: row; justify-content: space-around">
                                                <a href="/tasks/{{$task->id}}">
                                                    <span class="oi oi-eye" title="icon star" aria-hidden="true" style="color: blue"></span>
                                                </a>

                                                <form action="tasks/{{$task->id}}" method="POST">
                                                    {{method_field('DELETE')}}
                                                    @csrf
                                                    <button type="submit" style="border: none; background: transparent">
                                                        <span class="oi oi-trash" style="color: red"></span>
                                                    </button>
                                                </form>


                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                            {{ $tasks->appends((array) $params)->links() }}
                    </div>

                </div>

            </div>

        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Filter Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center mb-4">
                        <div class="col-lg-12">
                            <form action="/tasks" method="GET">
                                <fieldset>
                                    <div class="mt-4">
                                        <label for="name" class="col-lg-12 control-label">Name</label>
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" placeholder="name" name="name" value="{{isset($params->name)? $params->name: ''}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-lg-12 control-label">Owner</label>
                                        <div class="col-lg-12">
                                            {{Form::select('user_id', $input, isset($params->user_id)? $params->user_id: '', ['class' => 'form-control'])}}
                                        </div>
                                    </div>

                                    <div class="mt-4">
                                        <label for="name" class="col-lg-12 control-label">Email</label>
                                        <div class="col-lg-12">
                                            {{Form::select('email', $inputEmail, isset($params->email)? $params->email: '', ['class' => 'form-control'])}}
                                        </div>
                                    </div>

                                    <div class="mt-4">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
