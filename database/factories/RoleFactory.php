<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Role;
use Faker\Generator as Faker;

$roles = ['staff', 'sales', 'hr'];

foreach ($roles as $name) {
    $role = new Role();
    $role->name = $name;
    $role->save();
}

$users = \App\User::get();

